/*
Rebecca Kahn
rkahn
Lab 5
Lab Section: 4
Anurata Hridi
*/

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"

using namespace std;
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

typedef struct Card {
  Suit suit;
  int value;
} Card;

string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}


int main(int argc, char const *argv[]) {
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
  srand(unsigned (time(0)));


//card deck initialized as an array
  Card deck[52];
  int number = 0;

//for loop that assigns values 2-14 for every loop (4 loops) creating card deck
  for (int i = 0; i < 4; i++)
  { 
    for (int j = 2; j < 15; j++)
    {
      deck[number].suit = static_cast<Suit>(i);
      deck[number].value = j;
//allows the deck number to increase from 0-51 to fill the deck array with values
      number++;
    }
  }
      
//function with pointer to beginning of the deck array, pointer to 1 past the end of the  array, and pointer to the function myrandom
  random_shuffle(&deck[0], &deck[52], *myrandom);

//using the deck, a hand of 5 random cards is created
  Card firsthand[5] = {deck[0], deck[1], deck[2], deck[3], deck[4]};

//function that sorts the 5 random cards
  sort(&firsthand[0], &firsthand[5], suit_order);
  
//for loop that prints out the name and suit of the 5 random cards using the get_card_name and get_suit_code functions
  for (int c = 0; c < 5; c++)
  {
    cout << setw(10) << right << get_card_name(firsthand[c]) << " of " << get_suit_code(firsthand[c]) << "\n";
  }
  
  return 0;
}

//function takes in two  cards and returns true if lhs<rhs
bool suit_order(const Card& lhs, const Card& rhs) {
  if (lhs.suit < rhs.suit)
  {
    return true;
  }
  else if (lhs.suit == rhs.suit)
  {
    return lhs.value < rhs.value; 
  }
  else 
  {
    return false;
  }
}

//function of switch statements to assign the card its correct suit and image for printing out by returning their values
string get_suit_code(Card& c) {
  switch (c.suit) {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "";
  }
}

//function of switch statements that assign each card its value (either number of jack, queen, king, ace) and returns that value
string get_card_name(Card& c) {
  switch (c.value) {
    case 2: return "2";
    case 3: return "3";
    case 4: return "4";
    case 5: return "5";
    case 6: return "6";
    case 7: return "7";
    case 8: return "8";
    case 9: return "9";
    case 10: return "10";
    case 11: return "Jack";
    case 12: return "Queen";
    case 13: return "King";
    case 14: return "Ace";
    default: return "";
  }
}
























